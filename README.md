# Projet pac-man !

Projet de développement mobile.

## Récupération des fichiers

- Cloner ce dépôt git
- Utiliser GitKraken pour faciliter le travail

## Installation

- Installer Eclipse : http://www.eclipse.org/downloads/eclipse-packages/
- Installer le plugin ADT sur Eclipse : https://stuff.mit.edu/afs/sipb/project/android/docs/sdk/installing/installing-adt.html
- Dans Eclipse importer le projet avec Gradle via **file > import > Gradle > Existing Gradle Project**
- Aller dans Run => Run Configurations.. => choisir DesktopLauncher, dans l'onglet Arguments => Working Directory => cocher 'Others' puis entrer `${workspace_loc:Pac Man-desktop/assets}` et cliquer sur Apply et Run.

## Méthode de travail

Pour chaque modification du code, créer une branche à partir de master. Puis créer une merge request dans GitLab pour merger la branche crée sur master.