package com.pacman.game.model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.pacman.game.tools.Astar;
import com.pacman.game.tools.Node;
import com.pacman.game.view.TextureFactory;
import com.pacman.game.view.WorldRenderer;

public class Ghost2 extends Ghosts {
	
	public Ghost2(Vector2 position, World world) {
		super(position, world);
	}

	public TextureRegion getTexture(){
		return TextureFactory.iTexturable(this);
	}
	
	public void update(float delta){
		if(isDead){
			backToHome(delta);
		}
		else if(!leftHome){
			leaveHome(delta);
		}
		else{
			int Xf = 30-(Astar.roundPosX(this.position.x, this)/WorldRenderer.ppuX);
			int Yf = (Astar.roundPosY(this.position.y,this)/WorldRenderer.ppuX);
			int Xp = 30-(Astar.roundPosX(world.getPacman().position.x,world.getPacman())/WorldRenderer.ppuX);
			int Yp = (Astar.roundPosY(world.getPacman().position.y,world.getPacman())/WorldRenderer.ppuX); 
			int lab[][] = world.getMaze().labDemo;
			lab[Xf][Yf] = 3 ; 
			lab[Xp][Yp] = 4 ;
			
			Node nextNode = Astar.astar(lab,new Node(Xf,Yf)  ,new Node(Xp,Yp),this);
			
			if(nextNode != null){
				boolean continueOldDir = false ; 
				if(nextNode.getX() < Xf){
					if(tryUpMove(delta)){
						rightMove = leftMove = downMove = false;
						upMove = true;
						upMove(delta);
					}
					else
						continueOldDir = true;
				}
				if(nextNode.getX() > Xf){
					if(tryDownMove(delta)){
						rightMove = upMove = leftMove = false;
						downMove = true;
						downMove(delta);
					}
					else
						continueOldDir = true;
				}
				if(nextNode.getY() > Yf){
					if(tryRightMove(delta)){
						leftMove = upMove = downMove = false;
						rightMove = true;
						rightMove(delta);
					}
					else
						continueOldDir = true;
				}
				if(nextNode.getY() < Yf){
					if(tryLeftMove(delta)){
						rightMove = upMove = downMove = false;
						leftMove = true;
						leftMove(delta);
					}
					else
						continueOldDir = true;
				}
				if(continueOldDir){
					if(leftMove){
						leftMove(delta);
					}
					else if(rightMove){
						rightMove(delta);
					}
					else if(upMove){
						upMove(delta);
					}
					else if(downMove){
						downMove(delta);
					}
				}
			}
			else{
				if(leftMove){
					leftMove(delta);
				}
				else if(rightMove){
					rightMove(delta);
				}
				else if(upMove){
					upMove(delta);
				}
				else if(downMove){
					downMove(delta);
				}
			}
		}
	}
	
	@Override
	public void leaveHome(float delta) {
		pop(delta,5000);	
	}
}
