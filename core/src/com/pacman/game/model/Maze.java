package com.pacman.game.model;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.pacman.game.model.Block;
import com.pacman.game.model.GameElement;
import com.pacman.game.model.World;
import com.pacman.game.view.WorldRenderer;

public class Maze implements Iterable<GameElement>{
	private World world;
	private int height,width;
	private ArrayList<GameElement> blocks;
	private ArrayList<GameElement> gums;
	private ArrayList<GameElement> teleportation;
	public int labDemo[][] = { {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
			{1,5,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,5,1},
			{1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1},
			{1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,1},
			{1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,0,1,1,1,2,2,1,1,1,0,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,0,1,2,2,2,2,2,2,1,0,1,1,0,1,1,1,1,1,1},
			{0,0,0,0,0,0,0,0,0,0,1,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0},
			{1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1},
			{1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
			{1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
			{1,5,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,5,1},
			{1,1,1,0,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,0,1,1,1},
			{1,1,1,0,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,0,1,1,1},
			{1,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1},
			{1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
		  };
	
	public Maze(World world){
		this.world = world;
		this.height = 0;
		this.width = 0;
		blocks = new ArrayList<GameElement>();
		gums = new ArrayList<GameElement>();
		teleportation = new ArrayList<GameElement>();
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}
	
	public Block get(int a, int b){
		for(GameElement ge : blocks){
			if(ge.getWidth() == a && ge.getHeight() == b)
				return (Block) ge;
		}
		return null;
	}
	
	public void loadDemoLevel(){	
		for(int i = 0 ; i < 31 ; i++){
			for(int j = 0 ; j < 28 ; j++){
				if(labDemo[i][j] == 1){
					blocks.add(new Block(new Vector2(((30-i)*WorldRenderer.ppuX) ,WorldRenderer.ppuX*j), world));
				}
				if(labDemo[i][j] == 0){
					gums.add(new PacGum(new Vector2(((30-i)*WorldRenderer.ppuX) ,WorldRenderer.ppuX*j), world));
				}
				if(labDemo[i][j] == 5){
					gums.add(new SuperPacGum(new Vector2(((30-i)*WorldRenderer.ppuX) ,WorldRenderer.ppuX*j), world));
				}
				
			}
		}
		blocks.add(new DoorGhost(new Vector2((30-12)*WorldRenderer.ppuX,14*WorldRenderer.ppuX),world));
		blocks.add(new DoorGhost(new Vector2((30-12)*WorldRenderer.ppuX,13*WorldRenderer.ppuX),world));
		teleportation.add(new Block(new Vector2(((30-14)*WorldRenderer.ppuX) , -WorldRenderer.ppuX), world));
		teleportation.add(new Block(new Vector2(((30-14)*WorldRenderer.ppuX) ,WorldRenderer.ppuX*28), world));
	}
	
	public Iterator<GameElement> iterator(){		
		return blocks.iterator();
	}

	public ArrayList<GameElement> getTeleportation() {
		return teleportation;
	}

	public ArrayList<GameElement> getGommes() {
		return gums;
	}

}
