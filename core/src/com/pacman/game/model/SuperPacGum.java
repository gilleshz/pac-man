package com.pacman.game.model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.pacman.game.view.TextureFactory;

public class SuperPacGum extends GameElement {

	public float SIZE;
	public boolean isEaten;
	public SuperPacGum(Vector2 position, World world) {
		super(position, world);
		SIZE = 0;
		body = new Rectangle(position.x+7, position.y+7, 2, 2);
		isEaten = false;
	}
	
	public TextureRegion getTexture(){
		TextureFactory.getInstance();
		return TextureFactory.iTexturable(this);
	}
	
	public float getWidth(){
		return position.x;
	}
	
	public float getHeight(){
		return position.y;
	}
}