package com.pacman.game.model;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.pacman.game.model.GameElement;
import com.pacman.game.model.Maze;
import com.pacman.game.model.Pacman;
import com.pacman.game.view.WorldRenderer;

public class World implements Iterable<GameElement>{

	private Maze laby; 
	private Pacman pac;
	private Ghosts ghost1;
	private Ghosts ghost2;
	private Ghosts ghost3;
	public long timeStartFantomeEat;
	public long timeStart = System.currentTimeMillis();
	
	public World(){
	}
	
	public int getHeight(){
		return laby.getHeight() ;
	}
	
	public int getWidth(){
		return laby.getWidth();
	}

	public Maze getMaze() {
		return laby;
	}

	public Pacman getPacman() {
		return pac;
	}

	public Ghosts getGhost1() {
		return ghost1;
	}
	
	public Ghosts getGhost2() {
		return ghost2;
	}
	
	public Ghosts getGhost3() {
		return ghost3;
	}

	public Iterator<GameElement> iterator(){
		ArrayList<GameElement> l = new ArrayList<GameElement>();
				
		// Blocs du labyrinthe
		for(GameElement ge : laby){
			l.add(ge);
		}
		
		// Pac-Gomme
		for(GameElement ge : laby.getGommes())
			l.add(ge);
		
		// pacman
		l.add(pac);
		
		// Fantomes
		l.add(ghost1);
		l.add(ghost2);
		l.add(ghost3);
		
		return l.iterator();
	}
	
	public void createWorld(){
		laby = new Maze(this);
		ghost1 = new Ghost1(new Vector2((30-11)*WorldRenderer.ppuX,14*WorldRenderer.ppuX),this);
		ghost2 = new Ghost2(new Vector2((30-14)*WorldRenderer.ppuX,14*WorldRenderer.ppuX),this);
		ghost3 = new Ghost3(new Vector2((30-14)*WorldRenderer.ppuX,13*WorldRenderer.ppuX),this);
		pac = new Pacman(new Vector2((30-23)*WorldRenderer.ppuX,14*WorldRenderer.ppuX), this);
		laby.loadDemoLevel();
	}

	public void edibleGhost() {
		timeStartFantomeEat = System.currentTimeMillis();
		if(ghost1.leftHome)
			ghost1.isEdible = true;
		if(ghost2.leftHome)
			ghost2.isEdible = true;
		if(ghost3.leftHome)
			ghost3.isEdible = true;
	}
	
	public void testTempsFantomeMange(){
		long time = System.currentTimeMillis();
		if(time - timeStartFantomeEat >= 10000){
			ghost1.isEdible = false;
			ghost1.haveBeenEaten = false;
			ghost2.isEdible = false;
			ghost2.haveBeenEaten = false;
			ghost3.isEdible = false;
			ghost2.haveBeenEaten = false;
		}
		timeStart = time;
	}
	
	public boolean getTempsFinFantomeFaible(){
		return System.currentTimeMillis() - timeStartFantomeEat >= 8000;
	}
}
